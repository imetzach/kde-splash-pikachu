import QtQuick 2.9
Rectangle {
    id: root
    color: "#444444"

    AnimatedImage {
        id: loader
        source: "images/pikachu.gif"
        height: 322
        width: 451
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

}
