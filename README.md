# Pikachu Splashscreen
A simple splash screen for plasma kde that shows a **gif** of a running Pikachu

## Installation

To install just clone this project into ${HOME}/.local/share/plasma/look-and-feel

---

![Image](contents/previews/splash.png)


